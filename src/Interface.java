import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class Interface {
    @FXML
    ImageView equationOne;
    @FXML
    ImageView branchingImg;

    @FXML
    private void initialize() {
        equationOne.setImage(new Image("Image\\equationOne.png"));
        branchingImg.setImage(new Image("Image\\equationTwo.png"));
    }

    @FXML
    TextField linerX;
    @FXML
    TextField linerY;
    @FXML
    TextField linerZ;
    @FXML
    Button linearButton;
    @FXML
    Label linearAnswer;

    @FXML
    private void clickButtonLinear() {
        double x = Double.parseDouble(linerX.getText());
        double y = Double.parseDouble(linerY.getText());
        double z = Double.parseDouble(linerZ.getText());
        linearAnswer.setText(String.valueOf(EquationOne.calculation(x, y, z)));
    }


    @FXML
    TextField branchingX;
    @FXML
    TextField branchingY;
    @FXML
    Button branchingButton;
    @FXML
    Label branchingAnswer;

    String func = "cos";

    @FXML
    private void radButtonCos() {
        func = "cos";
    }

    @FXML
    private void radButtonSqr() {
        func = "sqr";
    }

    @FXML
    private void radButtonExm() {
        func = "exm";
    }

    @FXML
    private void clickButtonBranching() {
        double x = Double.parseDouble(branchingX.getText());
        double y = Double.parseDouble(branchingY.getText());
        System.out.println(func);
        branchingAnswer.setText(String.valueOf(EquationTwo.calculation(x, y, func)));
    }
}
