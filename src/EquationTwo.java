public class EquationTwo {
    public static double calculation(double x, double y, String func) {
        double function = 0;
        switch (func){
            case "cos":
                function= Math.cos(x);
                break;
            case "sqr":
                function = Math.pow(x,2);
                break;
            case "exp":
                function = Math.exp(x);
                break;
        }


        double answer = 0;

        if (x / y > 0) {
            double ctg = 1/Math.tan(y);
            answer = ctg + function;
        }else if (x / y < 0 ){
            answer= Math.log(y) + Math.tan(function);
        }else {
            answer = function * Math.pow(y,3);
        }
        return answer;
    }
}
