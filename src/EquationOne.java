public class EquationOne {
    public static double calculation(double x, double y, double z){
        double differenceCos = Math.cos(x) - Math.cos(y);
        if(differenceCos<0){
            differenceCos*=-1;
        }
        double cosPow = Math.pow(differenceCos,1+((1-Math.cos(2*y))/2));
        double amountDifferences = 1 + z + Math.pow(z,2)/2 + Math.pow(z,3)/3 + Math.pow(z,4);
        return cosPow * amountDifferences;
    }
}
