import org.junit.Test;

import static org.junit.Assert.*;

public class EquationTwoTest {

    @Test
    public void calculationEquationTwoPosCos() {
    double answer =-1.9271832255359689;
    double result = EquationTwo.calculation(5,9,"cos");
    assertEquals(answer,result,1);
    }
    @Test
    public void calculationEquationTwoPosSqr() {
        double answer =8.542342445639715;
        double result = EquationTwo.calculation(3,2,"sqr");
        assertEquals(answer,result,1);
    }
    @Test
    public void calculationEquationTwoNegCos() {
        double answer =-1.632085112534;
        double result = EquationTwo.calculation(-3,-1,"cos");
        assertEquals(answer,result,1);
    }
    @Test
    public void calculationEquationTwoNegSqr() {
        double answer = 36.45765755436029;
        double result = EquationTwo.calculation(-6,-2,"sqr");
        assertEquals(answer,result,1);
    }
    @Test
    public void calculationEquationTwoNegExp() {
        double answer = 0.1470639494805;
        double result = EquationTwo.calculation(-2,-8,"exp");
        assertEquals(answer,result,1);
    }
}