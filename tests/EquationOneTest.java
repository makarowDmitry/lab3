import org.junit.Test;

import static org.junit.Assert.*;

public class EquationOneTest {

    @Test
    public void calculEquationOnePositiv() {
    double answer = 30.266527015206;
    double result =EquationOne.calculation(2,4,4);
    assertEquals(answer,result,1);
    }

    @Test
    public  void calculEquationOneNegative(){
    double answer = 308.3221028381915;
    double result = EquationOne.calculation(-5,-2,-5);
    assertEquals(answer,result,1);
    }
    @Test
    public  void calculEquationOneFractionsPos(){
        double answer = 828.9680837308271;
        double result = EquationOne.calculation(5.23,4.78,8.1);
        assertEquals(answer,result,1);
    }
    @Test
    public  void calculEquationOneFractionsNeg(){
        double answer = 15.29161144428788;
        double result = EquationOne.calculation(-4.12,-8.8,-3.2);
        assertEquals(answer,result,1);
    }
}